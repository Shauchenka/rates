<?php

use core\Request;
use core\Router;

require_once 'vendor/autoload.php';

spl_autoload_register(function ($class) {
    $file = str_replace('\\', DIRECTORY_SEPARATOR, $class).'.php';
    if (file_exists($file)) {
        include $file;
        return true;
    }
    return false;
});


ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

echo "DEPLOYED ON ALL SERVERS NOW!!!!";
//
//$request = new Request();
//
//$router = Router::getInstance($request);
//
//$router->get('/show', 'BaseController@show');
//$router->post('/run', 'BaseController@run');
//$router->get('/edit/{id}', 'BaseController@edit');
//
//$router->dispatch();
