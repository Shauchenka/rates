<h3>Установка ранера</h3>

```bash
 sudo wget -O /usr/local/bin/gitlab-runner https://gitlab-runner-downloads.s3.amazonaws.com/latest/binaries/gitlab-runner-linux-amd64
 sudo chmod +x /usr/local/bin/gitlab-runner
 curl -sSL https://get.docker.com/ | sh
 sudo useradd --comment 'GitLab Runner' --create-home gitlab-runner --shell /bin/bash
 sudo gitlab-runner install --user=gitlab-runner --working-directory=/home/gitlab-runner
 sudo gitlab-runner start
```

<h3>Регистрация ранера</h3>

Путь к добавлению раннера из проекта на GitLab:  
```Settings - CI/CD - Runners - Expand ```

```bash
 sudo gitlab-runner register
 1. Please enter the gitlab-ci coordinator URL (e.g. https://gitlab.com )
 https://gitlab.com
 2. Please enter the gitlab-ci token for this runner (указан в настройках раннеров на гитлабе)
 3. Please enter the gitlab-ci description for this runner
 4. Please enter the gitlab-ci tags for this runner (comma separated) (Теги нужны для разделения ранеров. )
 5. Please enter the executor: ssh, docker+machine, docker-ssh+machine, kubernetes, docker, parallels, virtualbox, docker-ssh, shell (для наших задач нужен shell)
```

<h2>Раннер для одной машины настроен</h2>
Теперь он стучится на гитлаб и ждёт пока туда будет пуш

<h3>Запуск чего либо на раннере</h3>
Для каждего ранера добавляется примерно вот такая джоба в ```.gitlab-ci.yml``` 

```bash
first server job:
  stage:
    build
  tags:
    - myrunner # для каждего ранера свой тэг
  script:
    - echo "Hello, blead!"
    - cd /var/www/deploy/release
    - git pull
    - composer install --prefer-dist
    - echo "SUCCESS"

```

<h3>Проверка</h3>
Каждый ранер запускается сам по себе и в интерфейсе можно посмотреть статус и консоль каждогоиз них