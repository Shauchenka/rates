<?php
/**
 * Created by PhpStorm.
 * User: taras
 * Date: 06.04.19
 * Time: 13:07
 */

namespace core;

class Router
{
    private static $router = null;

    private $request;

    private $routes = [];

    /**
     * Router constructor.
     * @param $request
     */
    public function __construct(Request $request)
    {
        $this->request = $request;
    }

    public static function getInstance(?Request $request)
    {
        if (null === self::$router) {
            self::$router = new self($request);
        }
        return self::$router;
    }

    public function get($action, $callback)
    {
        $this->routes[Request::GET][$action] = $callback;
    }

    public function post($action,  $callback)
    {
        $this->routes[Request::POST][$action] = $callback;
    }

    public function dispatch()
    {
        $method = $this->request->getMethod();
        $parseResult = $this->parse();

        $action = $parseResult['action'];
        $params = $parseResult['params'];

        if(isset($this->routes[$method][$action])) {
            $callback = $this->routes[$method][$action];

            if($callback instanceof \Closure) {
                call_user_func($this->routes[$method][$action]);
            } else {
                $this->run($callback, $params);
            }

        } else {
            echo "404 блядь!";
        }
    }

    private function run(string $action, ?array $params = [])
    {
        list($class, $action) = explode('@', $action);
        $class = "src\\Controller\\" .$class;
        
        if(class_exists($class)) {
            $a = new $class;

            call_user_func_array([$a, $action], $params);
        } else {
            echo "class doesn't exists";
        }
    }

    private function parse()
    {
        $str = $this->request->getPath();
        if(strpos($str, '{') < 0) {
            return $str;
        }

        foreach ($this->routes["GET"] as $action => $callback) {
            $href = $action;
            $href = str_replace('/', '\/', $href);
            $href = str_replace('{id}', '\d+', $href);
            preg_match("/$href/", $str, $matches);

            if(isset($matches[0])) {
                preg_match('/\d+/', $matches[0], $params);

                return [
                    'action' => $action,
                    'params' => $params
                ];
            }
        }

        return null;
    }
}
