<?php

namespace core;

use GuzzleHttp\Client;
use GuzzleHttp\Cookie\CookieJar;
use GuzzleHttp\Cookie\SetCookie;
use GuzzleHttp\Promise\EachPromise;
use GuzzleHttp\Promise;
use GuzzleHttp\Pool;
use GuzzleHttp\Psr7\Request;
use GuzzleHttp\Cookie\SessionCookieJar;

class Multi
{
    public static $guzzle_init = null;

    public static function init_guzzle()
    {
        if (self::$guzzle_init == null) {
            self::$guzzle_init = new \GuzzleHttp\Client();
            return self::$guzzle_init;
        } else {
            return self::$guzzle_init;
        }
    }

    public static $tStart = 0;
    public static $tStartQ = 0;

    public static function Send($array)
    {
        self::$tStart = microtime(true);
        self::$tStartQ = microtime(true);
        $result = [];
        $client = self::init_guzzle();
        $requestsByAccount = [];
        $requests = function ($array) use ($client, &$requestsByAccount) {
            foreach ($array as $request) {
                //if (isset($request['login'])) {
                $requestsByAccount[$request['login']] = $request;
                //rt($request);//exit;
                if (!empty($request)) {
                    $cookiejar = new SessionCookieJar('SESSION_STORAGE_' . $request['login'], true);
                    if ($request['method'] == "POST") {

                        $data = [
                            'force_ip_resolve' => 'v4',
                            'form_params' => $request['curl_array'],
                            'cookies' => $cookiejar,
                            'connect_timeout' => 5.0,
                            'timeout' => 5.0,
                            'allow_redirects' => isset($request['allow_redirects']) ? $request['allow_redirects'] : true
                        ];

                        if (isset($request['header'])) if (is_array($request['header'])) {
                            $data['headers'] = $request['header'];
                        };

                        if (!isset($data['headers']['User-Agent'])) {
                            $data['headers']['User-Agent'] = 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36';
                        }

                        $url = $request['url'];
                        $index = $request['index'];
                        yield $index => function () use ($client, $data, $url) {
                            return $client->postAsync($url, $data);
                        };

                    } elseif ($request['method'] == "GET") {

                        $data = [
                            'force_ip_resolve' => 'v4',
                            'cookies' => $cookiejar,
                            'connect_timeout' => 3.14,
                            'timeout' => 3.14,
                            'allow_redirects' => isset($request['allow_redirects']) ? $request['allow_redirects'] : true
                        ];

                        if (isset($request['header'])) if (is_array($request['header'])) {
                            $data['headers'] = $request['header'];
                        };
                        if (!isset($data['headers']['User-Agent'])) {
                            $data['headers']['User-Agent'] = 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36';
                        }

                        //r($data);
                        $url = $request['url'];
                        if (isset($request['index'])) {
                            $index = $request['index'];
                            yield $index => function () use ($client, $data, $url) {
                                return $client->getAsync($url, $data);
                            };
                        }
                    } elseif ($request['method'] == "JSON") {

                        $data = [
                            'force_ip_resolve' => 'v4',
                            'json' => $request['curl_array'],
                            'cookies' => $cookiejar,
                            'connect_timeout' => 3.14,
                            'timeout' => 3.14,
                            'allow_redirects' => isset($request['allow_redirects']) ? $request['allow_redirects'] : true,
                            //'debug' => true
                        ];

                        if (isset($request['header'])) if (is_array($request['header'])) {
                            $data['headers'] = $request['header'];
                        };
                        //r($data);

                        $url = $request['url'];
                        $index = $request['index'];
                        yield $index => function () use ($client, $data, $url) {
                            return $client->postAsync($url, $data);
                        };
                    };
                }
                //}
            }
        };

        $repeatRequests = [];

        $pool = new Pool($client, $requests($array), [
            'concurrency' => 1,
            'fulfilled' => function ($response, $index) use (&$result, $repeatRequests, $requestsByAccount) {
                $statusCode = $response->getStatusCode();
                rt($statusCode);
                if ($statusCode != 200) {
                    $repeatRequests[] = $requestsByAccount[$index];
                    rt('Repeat: '. $index);
                } else {
                    $body = $response->getBody()->getContents();
                    $tS = microtime(true);
                    $tElapsedSecs = $tS - self::$tStart;
                    $tElapsedSecsQ = $tS - self::$tStartQ;
                    $sElapsedSecs = str_pad(number_format($tElapsedSecs, 3), 10, " ", STR_PAD_LEFT);
                    $sElapsedSecsQ = number_format($tElapsedSecsQ, 3);
                    self::$tStartQ = $tS;
                    $result[] = [
                        'index' => $index,
                        'html' => $body,
                        'time' => "Start Time: " . $sElapsedSecs . "s, Duration: " . $sElapsedSecsQ . "s"
                    ];
                }
            },
            'rejected' => function ($reason, $index) {
                //var_dump($reason, $index); exit;
                // this is delivered each failed request
            },
        ]);

        $promise = $pool->promise();
        $promise->wait();

        gc_collect_cycles();

        if (!empty($repeatRequests)) {
            $requests = function ($repeatRequests) use ($client) {
                foreach ($repeatRequests as $request) {
                    //if (isset($request['login'])) {
                    $requestsByAccount[$request['login']] = $request;
                    //rt($request);//exit;
                    if (!empty($request)) {
                        $cookiejar = new SessionCookieJar('SESSION_STORAGE_' . $request['login'], true);
                        if ($request['method'] == "POST") {

                            $data = [
                                'force_ip_resolve' => 'v4',
                                'form_params' => $request['curl_array'],
                                'cookies' => $cookiejar,
                                'connect_timeout' => 5.0,
                                'timeout' => 5.0,
                                'allow_redirects' => isset($request['allow_redirects']) ? $request['allow_redirects'] : true
                            ];

                            if (isset($request['header'])) if (is_array($request['header'])) {
                                $data['headers'] = $request['header'];
                            };

                            if (!isset($data['headers']['User-Agent'])) {
                                $data['headers']['User-Agent'] = 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36';
                            }

                            $url = $request['url'];
                            $index = $request['index'];
                            yield $index => function () use ($client, $data, $url) {
                                return $client->postAsync($url, $data);
                            };

                        } elseif ($request['method'] == "GET") {

                            $data = [
                                'force_ip_resolve' => 'v4',
                                'cookies' => $cookiejar,
                                'connect_timeout' => 3.14,
                                'timeout' => 3.14,
                                'allow_redirects' => isset($request['allow_redirects']) ? $request['allow_redirects'] : true
                            ];

                            if (isset($request['header'])) if (is_array($request['header'])) {
                                $data['headers'] = $request['header'];
                            };
                            if (!isset($data['headers']['User-Agent'])) {
                                $data['headers']['User-Agent'] = 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36';
                            }

                            //r($data);
                            $url = $request['url'];
                            if (isset($request['index'])) {
                                $index = $request['index'];
                                yield $index => function () use ($client, $data, $url) {
                                    return $client->getAsync($url, $data);
                                };
                            }
                        } elseif ($request['method'] == "JSON") {

                            $data = [
                                'force_ip_resolve' => 'v4',
                                'json' => $request['curl_array'],
                                'cookies' => $cookiejar,
                                'connect_timeout' => 3.14,
                                'timeout' => 3.14,
                                'allow_redirects' => isset($request['allow_redirects']) ? $request['allow_redirects'] : true,
                                //'debug' => true
                            ];

                            if (isset($request['header'])) if (is_array($request['header'])) {
                                $data['headers'] = $request['header'];
                            };
                            //r($data);

                            $url = $request['url'];
                            $index = $request['index'];
                            yield $index => function () use ($client, $data, $url) {
                                return $client->postAsync($url, $data);
                            };
                        };
                    }
                    //}
                }
            };

            $pool = new Pool($client, $requests($array), [
                'concurrency' => 1,
                'fulfilled' => function ($response, $index) use (&$result) {
                    $body = $response->getBody()->getContents();
                    $tS = microtime(true);
                    $tElapsedSecs = $tS - self::$tStart;
                    $tElapsedSecsQ = $tS - self::$tStartQ;
                    $sElapsedSecs = str_pad(number_format($tElapsedSecs, 3), 10, " ", STR_PAD_LEFT);
                    $sElapsedSecsQ = number_format($tElapsedSecsQ, 3);
                    self::$tStartQ = $tS;
                    $result[] = [
                        'index' => $index,
                        'html' => $body,
                        'time' => "Start Time: " . $sElapsedSecs . "s, Duration: " . $sElapsedSecsQ . "s"
                    ];
                },
                'rejected' => function ($reason, $index) {
                    //var_dump($reason, $index); exit;
                    // this is delivered each failed request
                },
            ]);

            $promise = $pool->promise();
            $promise->wait();
            gc_collect_cycles();
        }

        //echo json_encode($result); exit;

        return $result;
    }
}