<?php
/**
 * Created by PhpStorm.
 * User: taras
 * Date: 06.04.19
 * Time: 13:09
 */

namespace core;

use http\Exception\BadUrlException;

class Request
{
    const GET    = 'GET';
    const POST   = 'POST';
    const PUT    = 'PUT';
    const DELETE = 'DELETE';


    const REQUEST_METHODS = [
        self::GET,
        self::POST,
        self::PUT,
        self::DELETE
    ];

    private $requestUrl;
    private $requestMethod;
    private $path;
    private $getArgs;
    private $postArgs;
    private $method;

    function __construct()
    {
        if(!in_array($_SERVER['REQUEST_METHOD'], self::REQUEST_METHODS)) {
            throw new \Exception('unsupported request method');
        }
        $this->method = $_SERVER['REQUEST_METHOD'];

        $urlArr = parse_url($_SERVER['REQUEST_URI']);
        $args = $urlArr['query'] ?? "";

        $this->requestUrl = $_SERVER['REQUEST_URI'];
        $this->requestMethod = $_SERVER['REQUEST_METHOD'];
        $this->path = $urlArr['path'] ?? null;
        $this->getArgs = explode('&', $args);
    }

    /**
     * @return mixed
     */
    public function getRequestMethod()
    {
        return $this->requestMethod;
    }

    /**
     * @return mixed
     */
    public function getRequestUrl()
    {
        return $this->requestUrl;
    }

    /**
     * @return mixed
     */
    public function getMethod()
    {
        return $this->method;
    }

    /**
     * @return null
     */
    public function getPath()
    {
        return $this->path;
    }

    /**
     * @return array
     */
    public function getGetArgs(): array
    {
        return $this->getArgs;
    }

    public function getBody()
    {
        switch ($this->requestMethod) {
            case 'POST':
                $body = [];
                foreach($_POST as $key => $value)
                {
                    $body[$key] = filter_input(INPUT_POST, $key, FILTER_SANITIZE_SPECIAL_CHARS);
                }
                return $body;
            default:
                return [];
        }
    }

}